package com.vneuron.corebanking.presentation.transform;

import com.vneuron.corebanking.domain.form.Form;
import com.vneuron.corebanking.presentation.dto.FormDto;

public class FormMapper {

    public static FormDto toDto(Form form) {
        FormDto formDto = new FormDto();
        formDto.setId(form.getId());
        formDto.setCreatedOn(form.getCreatedOn());
        formDto.setIsEnabled(form.getIsEnabled());
        formDto.setCode(form.getCode());
        formDto.setContent(form.getContent());
        formDto.setDescription(form.getDescription());
        formDto.setName(form.getName());
        formDto.setStatus(form.getStatus());
        formDto.setType(form.getType());
        formDto.setUserId(form.getUserId());
        return formDto;
    }

    public static Form toEntity(FormDto formDto) {
        Form form = new Form();
        form.setId(formDto.getId());
        form.setCreatedOn(formDto.getCreatedOn());
        form.setIsEnabled(formDto.getIsEnabled());
        form.setCode(formDto.getCode());
        form.setContent(formDto.getContent());
        form.setDescription(formDto.getDescription());
        form.setName(formDto.getName());
        form.setStatus(formDto.getStatus());
        form.setType(formDto.getType());
        form.setUserId(formDto.getUserId());
        return form;
    }
}
