package com.vneuron.corebanking.presentation.transform;

import com.vneuron.corebanking.domain.account.AccountEntity;
import com.vneuron.corebanking.domain.transaction.Transaction;
import com.vneuron.corebanking.infrastructure.persistence.AccountRepository;
import com.vneuron.corebanking.presentation.dto.TransactionDto;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {

    @Autowired
    private AccountRepository accountRepository;
    public static TransactionDto toDto(Transaction transaction) {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setId(transaction.getId());
        transactionDto.setCreatedOn(transaction.getCreatedOn());
        transactionDto.setIsEnabled(transaction.getIsEnabled());
        transactionDto.setAmountOriginalCurrency(transaction.getAmountOriginalCurrency());
        transactionDto.setCurrency(transaction.getCurrency());
        transactionDto.setCustomerId(transaction.getCustomerId());
        transactionDto.setOperationDate(transaction.getOperationDate());
        transactionDto.setOperationNature(transaction.getOperationNature());
        transactionDto.setTransactionId(transaction.getTransactionId());
        transactionDto.setAccountId(transaction.getAccount().getId());
        transactionDto.setTransactionType(transaction.getTransactionType());
        return transactionDto;
    }

    public static Transaction toEntity(TransactionDto transactionDto,EntityManager entityManager) {
        Transaction transaction = new Transaction();
        transaction.setId(transactionDto.getId());
        transaction.setCreatedOn(transactionDto.getCreatedOn());
        transaction.setIsEnabled(transactionDto.getIsEnabled());
        transaction.setAmountOriginalCurrency(transactionDto.getAmountOriginalCurrency());
        transaction.setCurrency(transactionDto.getCurrency());
        transaction.setCustomerId(transactionDto.getCustomerId());
        transaction.setOperationDate(transactionDto.getOperationDate());
        transaction.setOperationNature(transactionDto.getOperationNature());
        transaction.setTransactionId(transactionDto.getTransactionId());
        transaction.setTransactionType(transactionDto.getTransactionType());
        // Set account reference
        if (transactionDto.getAccountId() != null) {
            AccountEntity account = entityManager.getReference(AccountEntity.class, transactionDto.getAccountId());
            transaction.setAccount(account);
        }
        return transaction;
    }
}
