package com.vneuron.corebanking.presentation.transform;

import com.vneuron.corebanking.domain.customer.CustomerEntity;

/**
 * Assembler class to convert the customer Resource Data to the customer model
 */
public class CustomerMapper {

    public static CustomerEntity toCustomerFromDTO(){
        return new CustomerEntity();
    }
}
