package com.vneuron.corebanking.presentation.transform;

import com.vneuron.corebanking.domain.user.UserEntity;
import com.vneuron.corebanking.infrastructure.security.services.UserDetailsImpl;
import com.vneuron.corebanking.presentation.dto.UserLoginResponseDto;
import com.vneuron.corebanking.presentation.dto.UserRegistrationRequestDto;
import org.springframework.security.core.GrantedAuthority;

import java.util.stream.Collectors;

public class UserMapper {

    public static UserEntity toEntity(UserRegistrationRequestDto userRegistrationRequestDto, String encodedPassword) {
        return new UserEntity(
                userRegistrationRequestDto.getUsername(),
                userRegistrationRequestDto.getEmail(),
                encodedPassword,
                userRegistrationRequestDto.getFirstname(),
                userRegistrationRequestDto.getLastname()
        );
    }

    public static UserLoginResponseDto toUserLoginResponseModel(String token, UserDetailsImpl userDetails) {
        return new UserLoginResponseDto(
                token,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                userDetails.getFirstName(),
                userDetails.getLastName(),
                userDetails.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toList())
        );
    }
}
