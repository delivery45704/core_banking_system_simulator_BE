package com.vneuron.corebanking.presentation.transform;


import com.vneuron.corebanking.domain.entity_type.EntityType;
import com.vneuron.corebanking.presentation.dto.EntityTypeDto;

public class EntityTypeMapper {

    public static EntityTypeDto toDto(EntityType entityType) {
        return new EntityTypeDto(entityType.getName(),entityType.getType());
    }

    public static EntityType toEntity(EntityTypeDto entityTypeDto) {
        return new EntityType(entityTypeDto.getName(),entityTypeDto.getType());
    }
}
