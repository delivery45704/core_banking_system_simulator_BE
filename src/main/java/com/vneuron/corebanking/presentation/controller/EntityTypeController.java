package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.entity_type.EntityTypeService;
import com.vneuron.corebanking.presentation.dto.EntityTypeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/entity-type")
public class EntityTypeController {

    private final Logger logger = LoggerFactory.getLogger(EntityTypeController.class);

    @Autowired
    EntityTypeService entityTypeService;

    @PostMapping("/")
    public ResponseEntity<EntityTypeDto> createForm(@RequestBody EntityTypeDto entityTypeDto) {
        EntityTypeDto savedEntityTypeDto = entityTypeService.saveEntityType(entityTypeDto);
        return ResponseEntity.ok(savedEntityTypeDto);
    }

    /**
     *
     * @return: the list of the entity types available
     */
    @GetMapping("/")
    public ResponseEntity<List<EntityTypeDto>> getAllForms() {
        List<EntityTypeDto> entityTypes = entityTypeService.getAllEntityTypes();
        return ResponseEntity.ok(entityTypes);
    }

    @DeleteMapping("/{name}")
    public ResponseEntity<Void> deleteEntityType(@PathVariable String name) {
        entityTypeService.deleteEntityType(name);
        return ResponseEntity.noContent().build();
    }
}
