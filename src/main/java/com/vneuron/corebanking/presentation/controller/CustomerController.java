package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.customer.CustomerService;
import com.vneuron.corebanking.domain.customer.CustomerCountByYear;
import com.vneuron.corebanking.domain.customer.CustomerEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping( "/list")
    public ResponseEntity<List<CustomerEntity>> getAllCustomers() {
        List<CustomerEntity> customers = customerService.getAllCustomers();
        return ResponseEntity.ok(customers);
    }

    @GetMapping( "/{id}")
    public ResponseEntity<CustomerEntity> getCustomerById(@PathVariable("id") long id) {
        CustomerEntity customers = (CustomerEntity) customerService.getCustomerById(id);
        return ResponseEntity.ok((CustomerEntity) customers);
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<CustomerEntity> saveCustomer(@PathVariable("id") long id, @RequestBody CustomerEntity customerEntity) {
        customerService.saveCustomer(customerEntity);
        return ResponseEntity.ok(customerEntity);
    }

    @PostMapping(value="/individal",consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> individual_create(@RequestBody CustomerEntity customerEntity) {
        CustomerEntity existingCustomer = customerService.findByEaiId(customerEntity.getEaiId());

        if (existingCustomer != null) {
            // Update the existing customer
            existingCustomer.setBusinessName(customerEntity.getBusinessName());
            existingCustomer.setCreatedOn(Timestamp.from(Instant.now()));
            existingCustomer.setActivity(customerEntity.getActivity());
            existingCustomer.setAge(customerEntity.getAge());
            existingCustomer.setBirthDate(customerEntity.getBirthDate());
            existingCustomer.setBirthPlace(customerEntity.getBirthPlace());
            existingCustomer.setCitizenship(customerEntity.getCitizenship());
            existingCustomer.setCurrentRiskRate(customerEntity.getCurrentRiskRate());
            existingCustomer.setCustomerStatus(customerEntity.getCustomerStatus());
            existingCustomer.setCustomerType(customerEntity.getCustomerType());
            existingCustomer.setEmail(customerEntity.getEmail());
            existingCustomer.setEntityType(customerEntity.getEntityType());
            existingCustomer.setFields0(customerEntity.getFields0());
            existingCustomer.setFields1(customerEntity.getFields1());
            existingCustomer.setFields2(customerEntity.getFields2());
            existingCustomer.setFields3(customerEntity.getFields3());
            existingCustomer.setFirstName(customerEntity.getFirstName());
            existingCustomer.setGender(customerEntity.getGender());
            existingCustomer.setLastName(customerEntity.getLastName());
            existingCustomer.setNationality(customerEntity.getNationality());
            existingCustomer.setNid(customerEntity.getNid());
            existingCustomer.setPasseport(customerEntity.getPasseport());
            existingCustomer.setProfession(customerEntity.getProfession());
            existingCustomer.setRiskValue(customerEntity.getRiskValue());
            existingCustomer.setSystem(customerEntity.getSystem());
            existingCustomer.setTin(customerEntity.getTin());
            existingCustomer.setWholeName(customerEntity.getWholeName());
            existingCustomer.setFormDataId(customerEntity.getFormDataId());
            existingCustomer.setEaiId(customerEntity.getEaiId());

            customerService.saveCustomer(existingCustomer);
        } else {
            // Create a new customer if it does not exist
            customerService.saveCustomer(customerEntity);
        }
        CustomerEntity lastCustomer = customerService.getLastInsertedCustomer();
        return ResponseEntity.ok("cbs_id : "+lastCustomer.getId());
    }

    @PostMapping(value ="/organization",consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> organization_create(@RequestBody CustomerEntity customerEntity) {

        CustomerEntity existingCustomer = customerService.findByEaiId(customerEntity.getEaiId());

        if (existingCustomer != null) {
            // Update the existing customer
            existingCustomer.setBusinessName(customerEntity.getBusinessName());
            existingCustomer.setCreatedOn(Timestamp.from(Instant.now()));
            existingCustomer.setActivity(customerEntity.getActivity());
            existingCustomer.setAge(customerEntity.getAge());
            existingCustomer.setBirthDate(customerEntity.getBirthDate());
            existingCustomer.setBirthPlace(customerEntity.getBirthPlace());
            existingCustomer.setCitizenship(customerEntity.getCitizenship());
            existingCustomer.setCurrentRiskRate(customerEntity.getCurrentRiskRate());
            existingCustomer.setCustomerStatus(customerEntity.getCustomerStatus());
            existingCustomer.setCustomerType(customerEntity.getCustomerType());
            existingCustomer.setEmail(customerEntity.getEmail());
            existingCustomer.setEntityType(customerEntity.getEntityType());
            existingCustomer.setFields0(customerEntity.getFields0());
            existingCustomer.setFields1(customerEntity.getFields1());
            existingCustomer.setFields2(customerEntity.getFields2());
            existingCustomer.setFields3(customerEntity.getFields3());
            existingCustomer.setFirstName(customerEntity.getFirstName());
            existingCustomer.setGender(customerEntity.getGender());
            existingCustomer.setLastName(customerEntity.getLastName());
            existingCustomer.setNationality(customerEntity.getNationality());
            existingCustomer.setNid(customerEntity.getNid());
            existingCustomer.setPasseport(customerEntity.getPasseport());
            existingCustomer.setProfession(customerEntity.getProfession());
            existingCustomer.setRiskValue(customerEntity.getRiskValue());
            existingCustomer.setSystem(customerEntity.getSystem());
            existingCustomer.setTin(customerEntity.getTin());
            existingCustomer.setWholeName(customerEntity.getWholeName());
            existingCustomer.setFormDataId(customerEntity.getFormDataId());
            existingCustomer.setEaiId(customerEntity.getEaiId());

            customerService.saveCustomer(existingCustomer);
        } else {
            // Create a new customer if it does not exist
            customerService.saveCustomer(customerEntity);
        }
        CustomerEntity lastCustomer = customerService.getLastInsertedCustomer();
        return ResponseEntity.ok("cbs_id : "+lastCustomer.getId());
    }

    @DeleteMapping(value = "/{id}/delete", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Void> deleteUser(@PathVariable("id") long id) {
        customerService.deleteCustomer(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping( "/count")
    public ResponseEntity<Integer> getAllCustomersNumber() {
        List<CustomerEntity> customers = customerService.getAllCustomers();
        return ResponseEntity.ok(customers.size());
    }

    @GetMapping( "/entity_type/count")
    public ResponseEntity<int[]> getCustomersCountByEntity() {
        int pp_count = customerService.findByEntityType("PP");
        int pm_count = customerService.findByEntityType("PM");
        int[] customers;
        customers = new int[]{pp_count,pm_count};
        return ResponseEntity.ok(customers);
    }

    @GetMapping( "/year/count")
    public List<CustomerCountByYear> getCustomersCountByYear() {
        List<CustomerCountByYear> results = customerService.findCountCustomersByYear();


        return results;
    }
}
