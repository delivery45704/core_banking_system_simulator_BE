package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.entity_type.EntityTypeService;
import com.vneuron.corebanking.application.form.FormEntityTypeMapService;
import com.vneuron.corebanking.application.form.FormService;
import com.vneuron.corebanking.domain.entity_type.EntityType;
import com.vneuron.corebanking.domain.form.Form;
import com.vneuron.corebanking.presentation.dto.FormEntityMappingDTO;
import com.vneuron.corebanking.presentation.utils.ApiResponse;
import com.vneuron.corebanking.presentation.utils.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/form-entity-type-mapping")
public class FormEntityTypeMapController {

    private final Logger logger = LoggerFactory.getLogger(FormEntityTypeMapController.class);

    @Autowired
    private FormEntityTypeMapService formEntityTypeMapService;

    @Autowired
    private FormService formService;

    @Autowired
    private EntityTypeService entityTypeService;

    /**
     * Get all mappings.
     *
     * @return List of mappings.
     */
    @GetMapping("/")
    public ResponseEntity<List<FormEntityMappingDTO>> getAllMappings() {
        List<FormEntityMappingDTO> mappings = formEntityTypeMapService.getAllMappings();
        return ResponseEntity.ok(mappings);
    }
    // Create a new mapping between Form and EntityType
    @PostMapping("/map")
    public ResponseEntity<?> createMapping(@RequestBody FormEntityMappingDTO formEntityMappingDTO) throws ResourceNotFoundException {
        // Find the Form and EntityType by their codes/names
        Form form = formService.findByCode(formEntityMappingDTO.getFormCode());
        EntityType entityType = entityTypeService.findByName(formEntityMappingDTO.getEntityTypeName());

        // Check if either the form or entity type is not found
        if (form == null || entityType == null) {
            ApiResponse<Void> response = new ApiResponse<>("Form or EntityType not found", "BAD_REQUEST", HttpStatus.BAD_REQUEST.value());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        // Check if the mapping already exists
        if (formEntityTypeMapService.isMappingExists(formEntityMappingDTO.getFormCode(), formEntityMappingDTO.getEntityTypeName())) {
            ApiResponse<Void> response = new ApiResponse<>("Mapping already exists", "CONFLICT", HttpStatus.CONFLICT.value());
            return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
        }

        // Create and save the mapping
        formEntityTypeMapService.mapFormToEntityType(formEntityMappingDTO.getFormCode(), formEntityMappingDTO.getEntityTypeName());

        ApiResponse<Void> response = new ApiResponse<>("Mapping created successfully.", null, HttpStatus.CREATED.value());
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    // Delete a mapping
    @DeleteMapping("/unmap")
    public ResponseEntity<?> deleteMapping(@RequestParam String formCode, @RequestParam String entityTypeName) throws ResourceNotFoundException {
        if (!formEntityTypeMapService.isMappingExists(formCode, entityTypeName)) {
            ApiResponse<Void> response = new ApiResponse<>("Mapping not found.", "NOT_FOUND", HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }

        try {
            // Delete the mapping
            formEntityTypeMapService.deleteMapping(formCode, entityTypeName);

            // Return success response
            ApiResponse<Void> response = new ApiResponse<>("Mapping deleted successfully.", null, HttpStatus.OK.value());
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            // Return error response in case of failure
            ApiResponse<Void> response = new ApiResponse<>("Failed to delete mapping.", "INTERNAL_SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR.value());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }


}
