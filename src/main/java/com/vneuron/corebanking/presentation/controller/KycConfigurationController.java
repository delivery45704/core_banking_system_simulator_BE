package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.kyc_third_party_config.KycConfigurationService;
import com.vneuron.corebanking.domain.kyc_third_party_config.KycThirdPartyConfiguration;
import com.vneuron.corebanking.presentation.utils.ApiResponse;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/kyc")
public class KycConfigurationController {

    private final Logger logger = LoggerFactory.getLogger(KycConfigurationController.class);

    private final KycConfigurationService service;


    public KycConfigurationController(KycConfigurationService service) {
        this.service = service;
    }

    /**
     * Get KYC third-party configurations.
     *
     * @return KYC configurations.
     */
    @GetMapping("/configuration")
    public ResponseEntity<ApiResponse<KycThirdPartyConfiguration>> getKycConfigurations() {
        KycThirdPartyConfiguration configuration = service.getKycConfigurations();
        return ResponseEntity.ok(new ApiResponse<>("Configuration fetched successfully", configuration, HttpStatus.OK.value()));
    }

    /**
     *
     * @param configuration is a record contains the configuration needed
     * @return status 201 for creation
     */
    @PostMapping("/configuration")
    public ResponseEntity<?> saveKycConfiguration(@Valid @RequestBody KycThirdPartyConfiguration configuration) {
        service.saveKycConfiguration(configuration);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse<>("Configuration saved successfully", null, HttpStatus.CREATED.value()));
    }
}
