package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.transaction.TransactionService;
import com.vneuron.corebanking.presentation.dto.FormDto;
import com.vneuron.corebanking.presentation.dto.TransactionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/transactions")
public class TransactionController {

    private final Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    private TransactionService transactionService;

    @PostMapping("/")
    public ResponseEntity<TransactionDto> createTransaction(@RequestBody TransactionDto transactionDto) {
        TransactionDto savedFormDto = transactionService.saveTransaction(transactionDto);
        return ResponseEntity.ok(savedFormDto);
    }

    /**
     * @return the list of the transactions fully
     */
    @GetMapping("/")
    public ResponseEntity<List<TransactionDto>> getAllForms() {
        List<TransactionDto> forms = transactionService.getAllTransactions();
        return ResponseEntity.ok(forms);
    }

    /**
     * method dedicated for the usage of pagination
     *
     * @param page: is the number of the page you want to retrieve(set to 0 with small volume of data)
     * @param size: is the size of each page
     * @return : list of transactions paginated
     */
    @GetMapping("/all")
    public ResponseEntity<Page<TransactionDto>> getAllForms(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        Page<TransactionDto> transactionListDto = transactionService.getAllTransactions(page, size);
        return ResponseEntity.ok(transactionListDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TransactionDto> getTransactionById(@PathVariable Long id) {
        TransactionDto transactionDto = transactionService.getTransactionById(id);
        if (transactionDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(transactionDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTransaction(@PathVariable Long id) {
        transactionService.deleteTransaction(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/count")
    public ResponseEntity<Integer> getAllFormsNumber() {
        List<TransactionDto> transactionListDto = transactionService.getAllTransactions();
        return ResponseEntity.ok(transactionListDto.size());
    }
}
