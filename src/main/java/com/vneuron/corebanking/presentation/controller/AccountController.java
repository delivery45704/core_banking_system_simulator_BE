package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.account.AccountService;
import com.vneuron.corebanking.domain.account.AccountEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/account")
public class AccountController {

    private final AccountService accountService;

    @GetMapping( "/list")
    public ResponseEntity<List<AccountEntity>> getAllAccounts() {
        List<AccountEntity> account = accountService.getAllAccounts();
        return ResponseEntity.ok(account);
    }

    @GetMapping( "/{id}")
    public ResponseEntity<AccountEntity> getAccountById(@PathVariable("id") long id) {
        AccountEntity account = accountService.getAccountById(id);
        return ResponseEntity.ok(account);
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<AccountEntity> saveAccount(@PathVariable("id") long id, @RequestBody AccountEntity AccountEntity) {
        accountService.saveAccount(AccountEntity);
        return ResponseEntity.ok(AccountEntity);
    }


    @DeleteMapping(value = "/{id}/delete", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Void> deleteAccount(@PathVariable("id") long id) {
        accountService.deleteAccount(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping( "/count")
    public ResponseEntity<Integer> getAllAccountsNumber() {
        List<AccountEntity> account = accountService.getAllAccounts();
        return ResponseEntity.ok(account.size());
    }
}
