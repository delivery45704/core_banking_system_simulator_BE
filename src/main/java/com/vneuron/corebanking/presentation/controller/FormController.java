package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.form.FormService;
import com.vneuron.corebanking.presentation.dto.FormDto;
import com.vneuron.corebanking.presentation.utils.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/forms")
public class FormController {

    private final Logger logger = LoggerFactory.getLogger(FormController.class);

    @Autowired
    private FormService formService;

    @PostMapping("/")
    public ResponseEntity<Object> createForm(@RequestBody FormDto formDto) {
        if (formService.isCodeExist(formDto.getCode())){
            // Return 409 Conflict if the code is already in use
            ErrorResponse errorResponse = new ErrorResponse("The code '" + formDto.getCode() + "' is already in use. Please provide a unique code.", "CODE_ALREADY_EXISTS");
            return ResponseEntity.status(HttpStatus.CONFLICT).body(errorResponse);
        }
        FormDto savedFormDto = formService.saveForm(formDto);
        return ResponseEntity.ok(savedFormDto);
    }

    /**
     *
     * @return: the list of the forms
     */
    @GetMapping("/")
    public ResponseEntity<List<FormDto>> getAllForms() {
        List<FormDto> forms = formService.getAllForms();
        return ResponseEntity.ok(forms);
    }

    /**
     *  method dedicated for the usage of pagination
     * @param page: is the number of the page you want to retrieve(set to 0 with small volume of data)
     * @param size: is the size of each page
     * @return : list of forms paginated
     */
    @GetMapping("/all")
    public ResponseEntity<Page<FormDto>> getAllForms(@RequestParam int page, @RequestParam int size) {
        Page<FormDto> forms = formService.getAllForms(page,size);
        logger.info("size= "+forms.toString());
        return ResponseEntity.ok(forms);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FormDto> getFormById(@PathVariable Long id) {
        FormDto formDto = formService.getFormById(id);
        if (formDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(formDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteForm(@PathVariable Long id) {
        formService.deleteForm(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/")
    public ResponseEntity<Void> deleteForms(@RequestBody List<Long> ids) {
        formService.deleteForms(ids);
        return ResponseEntity.noContent().build();
    }
    @PutMapping("/{id}")
    public ResponseEntity<FormDto> updateForm(@PathVariable Long id, @RequestBody FormDto formDto){
        FormDto updatedFormDto = formService.updateForm(id, formDto);
        if (updatedFormDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedFormDto);
    }

}
