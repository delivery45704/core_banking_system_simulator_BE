package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.user.UserService;
import com.vneuron.corebanking.domain.user.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService;

    @GetMapping( "/list")
    public ResponseEntity<List<UserEntity>> getAllUsers() {
        List<UserEntity> users = userService.getAllUsers();
        return ResponseEntity.ok(users);
    }

    @GetMapping( "/{id}")
    public ResponseEntity<UserEntity> getUserById(@PathVariable("id") long id) {
        UserEntity user = userService.getUserById(id);
        return ResponseEntity.ok(user);
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<UserEntity> saveUser(@PathVariable("id") long id, @RequestBody UserEntity userEntity) {
        userService.saveUser(userEntity);
        return ResponseEntity.ok(userEntity);
    }

    @DeleteMapping(value = "/{id}/delete", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Void> deleteUser(@PathVariable("id") long id) {
        UserEntity user = userService.getUserById(id);
        user.getRoles().clear();
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }
}
