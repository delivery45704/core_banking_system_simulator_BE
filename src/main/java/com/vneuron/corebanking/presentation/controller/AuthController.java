package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.authentication.AuthenticationService;
import com.vneuron.corebanking.application.registration.RegistrationService;
import com.vneuron.corebanking.domain.user.UserEntity;
import com.vneuron.corebanking.presentation.dto.UserLoginRequestDto;
import com.vneuron.corebanking.presentation.dto.UserRegistrationRequestDto;
import com.vneuron.corebanking.presentation.dto.UserLoginResponseDto;
import com.vneuron.corebanking.presentation.dto.UserRegistrationResponseDto;
import com.vneuron.corebanking.presentation.transform.UserMapper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final static Logger logger = LoggerFactory.getLogger(AuthController.class);

    private final RegistrationService registrationService;

    private final AuthenticationService authenticationService;

    private final PasswordEncoder encoder;

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody UserLoginRequestDto userLoginRequestDto){
        logger.debug("authenticateUser: {}", userLoginRequestDto);
        UserLoginResponseDto response = authenticationService.authenticateUser(
                userLoginRequestDto.getUsername(),
                userLoginRequestDto.getPassword()
        );
        return ResponseEntity.ok(response);
    }


    @PostMapping( "/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserRegistrationRequestDto userRegistrationRequestDto){
        logger.debug("registerUser: {}", userRegistrationRequestDto);
        String encodedPassword = encoder.encode(userRegistrationRequestDto.getPassword());
        UserEntity userEntity = UserMapper.toEntity(userRegistrationRequestDto, encodedPassword);

        UserRegistrationResponseDto response = registrationService.registerUser(userEntity, userRegistrationRequestDto.getRole());

        if (response.getMessage().startsWith("Error")) {
            return ResponseEntity.badRequest().body(response);
        }

        return ResponseEntity.ok(response);

    }

    @GetMapping("/all")
    public String allAccess() {
        return "Public Content.";
    }
}
