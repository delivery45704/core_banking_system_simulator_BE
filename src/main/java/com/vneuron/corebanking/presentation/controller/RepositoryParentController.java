package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.repository.RepositoryParentService;
import com.vneuron.corebanking.domain.repository.RepositoryParent;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/parent_repository")
public class RepositoryParentController {
    private final RepositoryParentService repositoryParentService;


    @GetMapping( "/list")
    public ResponseEntity<List<RepositoryParent>> getAllParentRepositorys() {
        List<RepositoryParent> ParentRepositorys = repositoryParentService.getAllRepositorys();
        return ResponseEntity.ok(ParentRepositorys);
    }

    @GetMapping( "/{id}")
    public ResponseEntity<RepositoryParent> getParentRepositoryById(@PathVariable("id") long id) {
        RepositoryParent ParentRepository = repositoryParentService.getRepositoryById(id);
        return ResponseEntity.ok(ParentRepository);
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<RepositoryParent> saveParentRepository(@PathVariable("id") long id, @RequestBody RepositoryParent ParentRepositoryEntity) {
        repositoryParentService.saveRepository(ParentRepositoryEntity);
        return ResponseEntity.ok(ParentRepositoryEntity);
    }

    @DeleteMapping(value = "/{id}/delete", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Void> deleteParentRepository(@PathVariable("id") long id) {
        RepositoryParent parentRepository = repositoryParentService.getRepositoryById(id);

        if (parentRepository == null) {
            return ResponseEntity.notFound().build(); // Return 404 if not found
        }

        // Clear child references
        parentRepository.getChilds().forEach(child -> {
            child.setParent(null); // Assuming you have a setParent method to avoid FK constraint
        });
        parentRepository.getChilds().clear(); // Clear the list

        // Now delete the parent
        repositoryParentService.deleteRepository(id);

        return ResponseEntity.noContent().build();
    }


}
