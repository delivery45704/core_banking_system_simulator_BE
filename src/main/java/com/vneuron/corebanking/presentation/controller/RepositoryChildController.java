package com.vneuron.corebanking.presentation.controller;

import com.vneuron.corebanking.application.repository.RepositoryChildService;
import com.vneuron.corebanking.application.repository.RepositoryParentService;
import com.vneuron.corebanking.domain.repository.RepositoryChild;
import com.vneuron.corebanking.domain.repository.RepositoryParent;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/{parent_id}/Child_repository")
public class RepositoryChildController {
    private final RepositoryChildService RepositoryChildService;
    private final RepositoryParentService RepositoryParentService;


    @GetMapping( "/list")
    public ResponseEntity<List<RepositoryChild>> getAllParentRepositorys(@PathVariable("parent_id") String id) {
        List<RepositoryChild> ParentRepositorys = RepositoryChildService.getRepositoryChildsById(id);
        return ResponseEntity.ok(ParentRepositorys);
    }

    @GetMapping( "/{id}")
    public ResponseEntity<RepositoryChild> getParentRepositoryById(@PathVariable("id") long id) {
        RepositoryChild ParentRepository = RepositoryChildService.getRepositoryChildById(id);
        return ResponseEntity.ok(ParentRepository);
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<RepositoryChild> saveParentRepository(@PathVariable("id") long id,@PathVariable("parent_id") long parent_id, @RequestBody RepositoryChild ChildRepositoryEntity) {
        RepositoryParent rp = RepositoryParentService.getRepositoryById(parent_id);
        ChildRepositoryEntity.setParent(rp);
        RepositoryChildService.saveRepositoryChild(ChildRepositoryEntity);
        return ResponseEntity.ok(ChildRepositoryEntity);
    }

    @DeleteMapping(value = "/{id}/delete", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Void> deleteParentRepository(@PathVariable("id") long id) {
        RepositoryChild childRepository = RepositoryChildService.getRepositoryChildById(id);
        if (childRepository == null) {
            return ResponseEntity.notFound().build(); // Return 404 if not found
        }
        RepositoryChildService.deleteRepositoryChild(id);
        return ResponseEntity.noContent().build();
    }

}
