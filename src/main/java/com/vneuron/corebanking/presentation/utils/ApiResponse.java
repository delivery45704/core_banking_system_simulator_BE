package com.vneuron.corebanking.presentation.utils;

public class ApiResponse<T> {
    private String message;
    private T data;
    private String error;
    private int status;

    // Constructors
    public ApiResponse(String message, T data, int status) {
        this.message = message;
        this.data = data;
        this.status = status;
    }

    public ApiResponse(String message, String error, int status) {
        this.message = message;
        this.error = error;
        this.status = status;
    }

    // Getters and setters
    public String getMessage() { return message; }
    public void setMessage(String message) { this.message = message; }
    public T getData() { return data; }
    public void setData(T data) { this.data = data; }
    public String getError() { return error; }
    public void setError(String error) { this.error = error; }
    public int getStatus() { return status; }
    public void setStatus(int status) { this.status = status; }
}
