package com.vneuron.corebanking.presentation.dto;

public class UserRegistrationResponseDto {

    private String message;

    public UserRegistrationResponseDto(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
