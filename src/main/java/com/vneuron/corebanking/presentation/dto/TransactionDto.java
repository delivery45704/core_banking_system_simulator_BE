package com.vneuron.corebanking.presentation.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Setter
@Getter
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class TransactionDto {
    private Long id;

    private BigDecimal amountOriginalCurrency;

    private Timestamp createdOn;

    private String currency;

    private Long  customerId;

    private Boolean isEnabled = true;

    private Timestamp operationDate;

    private String operationNature;

    private UUID transactionId;

    private String transactionType;

    private Long accountId;
}
