package com.vneuron.corebanking.presentation.dto;


import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class FormEntityMappingDTO {

    private String formCode;
    private String entityTypeName;

    public FormEntityMappingDTO(String formCode, String entityTypeName) {
        this.formCode = formCode;
        this.entityTypeName = entityTypeName;
    }

    // Getters and Setters
    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public String getEntityTypeName() {
        return entityTypeName;
    }

    public void setEntityTypeName(String entityTypeName) {
        this.entityTypeName = entityTypeName;
    }
}
