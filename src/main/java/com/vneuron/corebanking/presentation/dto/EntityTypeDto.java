package com.vneuron.corebanking.presentation.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.vneuron.corebanking.domain.entity_type.ECustomerType;
import lombok.Getter;
import lombok.Setter;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class EntityTypeDto {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private ECustomerType type;

    public EntityTypeDto(String name, ECustomerType type) {
        this.name = name;
        this.type = type;
    }

    public EntityTypeDto() {
    }
}
