package com.vneuron.corebanking.presentation.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.vneuron.corebanking.domain.entity_type.ECustomerType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class FormDto {
    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    private LocalDateTime createdOn = LocalDateTime.now();
    @Getter
    @Setter
    private Boolean isEnabled = true;
    @Getter
    @Setter
    private String code;
    @Getter
    @Setter
    private String content;
    @Getter
    @Setter
    private String description;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private Integer status;
    @Getter
    @Setter
    private ECustomerType type;
    @Getter
    @Setter
    private Long userId;



}
