package com.vneuron.corebanking.presentation.dto;

import com.vneuron.corebanking.domain.user.UserEntity;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import com.vneuron.corebanking.infrastructure.persistence.UserCrudRepository;
import java.util.*;

public class UsersInfoRequestModel {
    @NotBlank
    @Size(min = 3, max = 40)
    private String username;

    @NotBlank
    @Size(min = 6, max = 150)
    private String password;

    @NotBlank
    @Size( max = 100)
    private String firstname;

    @NotBlank
    @Size(max = 100)
    private String lastname;

    @NotBlank
    @Size(min = 3, max = 50)
    private String email;

    private Set<String> role;

}
