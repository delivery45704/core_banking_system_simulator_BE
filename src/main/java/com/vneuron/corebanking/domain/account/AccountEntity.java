package com.vneuron.corebanking.domain.account;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vneuron.corebanking.domain.customer.CustomerEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;

@Entity
@Table(name = "contracts")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "created_on", nullable = false)
    private Timestamp createdOn;

    @Column(name = "is_enabled")
    private Boolean isEnabled=true;

    
    @Column(name = "account_status")
    private String accountStatus;

    
    @Column(name = "contract_balance")
    private BigDecimal contractBalance;

    
    @Column(name = "currency")
    private String currency;


    @ManyToOne(fetch = FetchType.EAGER )
    @JoinColumn(name = "customer_id", nullable = false)
    @JsonBackReference
    private CustomerEntity customer;

    @Column(name = "num_contract", nullable = false)
    private String numContract;

    
    @Column(name = "opened_date")
    private Timestamp openedDate;

    
    @Column(name = "termination_date")
    private Timestamp terminationDate;


    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    @PrePersist
    protected void onCreate() {
        this.createdOn = Timestamp.from(Instant.now());
    }

    public AccountEntity(){}
    public AccountEntity(Timestamp createdOn, Boolean isEnabled, String accountStatus, BigDecimal contractBalance, String currency, CustomerEntity customer, String numContract, Timestamp openedDate, Timestamp terminationDate) {
        this.createdOn = createdOn;
        this.isEnabled = isEnabled;
        this.accountStatus = accountStatus;
        this.contractBalance = contractBalance;
        this.currency = currency;
        this.customer = customer;
        this.numContract = numContract;
        this.openedDate = openedDate;
        this.terminationDate = terminationDate;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public BigDecimal getContractBalance() {
        return contractBalance;
    }

    public String getCurrency() {
        return currency;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public long getCustomerId() {
        return customer.getId();
    }

    public String getCustomerBusinessName() {
        return customer.getBusinessName();
    }

    public String getCustomerFirstName() {
        return customer.getFirstName();
    }

    public String getCustomerLastName() {
        return customer.getLastName();
    }

    public String getNumContract() {
        return numContract;
    }

    public Timestamp getOpenedDate() {
        return openedDate;
    }

    public Timestamp getTerminationDate() {
        return terminationDate;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public void setContractBalance(BigDecimal contractBalance) {
        this.contractBalance = contractBalance;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public void setNumContract(String numContract) {
        this.numContract = numContract;
    }

    public void setOpenedDate(Timestamp openedDate) {
        this.openedDate = openedDate;
    }

    public void setTerminationDate(Timestamp terminationDate) {
        this.terminationDate = terminationDate;
    }

    public Long getId() {
        return id;
    }
}
