package com.vneuron.corebanking.domain.transaction;
import com.vneuron.corebanking.domain.account.AccountEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;


@Entity
@Table(name = "transactions")
public class Transaction {

    @Setter
    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Setter
    @Getter
    @Column(name = "amount_original_currency")
    private BigDecimal amountOriginalCurrency;

    @Setter
    @Getter
    @Column(name = "created_on", nullable = false)
    private Timestamp createdOn;

    @Setter
    @Getter
    @Column(name = "currency")
    private String currency;

    @Setter
    @Getter
    @Column(name = "customer_id")
    private Long  customerId;

    @Setter
    @Getter
    @Column(name = "is_enabled")
    private Boolean isEnabled = true;

    @Setter
    @Getter
    @Column(name = "operation_date", nullable = false)
    private Timestamp operationDate;

    @Setter
    @Getter
    @Column(name = "operation_nature")
    private String operationNature;

    @Setter
    @Getter
    @Column(name = "transaction_id")
    private UUID transactionId;

    @Setter
    @Getter
    @Column(name = "transaction_type")
    private String transactionType;

    @Setter
    @Getter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", nullable = false)
    private AccountEntity account;


    @PrePersist
    protected void onCreate() {
        this.createdOn = Timestamp.from(Instant.now());
        if (this.id == null) this.transactionId = UUID.randomUUID();
    }

}
