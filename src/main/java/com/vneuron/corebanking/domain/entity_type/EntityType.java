package com.vneuron.corebanking.domain.entity_type;

import com.vneuron.corebanking.domain.form.Form;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "entity_types")
public class EntityType {

    @Id
    @Column(nullable = false, unique = true)
    @Setter
    @Getter
    private String name;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @Setter
    @Getter
    private ECustomerType type;

    @Getter
    @Setter
    @ManyToMany
    @JoinTable(
            name = "form_entity_type",
            joinColumns = @JoinColumn(name = "entity_type_id"),
            inverseJoinColumns = @JoinColumn(name = "form_id")
    )
    private Set<Form> forms = new HashSet<>();

    public EntityType(){
    }

    public EntityType(String name, ECustomerType type) {
        this.name = name;
        this.type = type;
    }



    // toString, equals, and hashCode
    @Override
    public String toString() {
        return "EntityType{" +
                "name='" + name + '\'' +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityType that = (EntityType) o;

        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
