package com.vneuron.corebanking.domain.repository;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import javax.xml.bind.annotation.XmlElement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "parent_repository", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")
})
@Getter
@Setter
public class RepositoryParent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 40)
    @Column(nullable = false)
    private String name;

    @Size(max = 50)
    @Column(nullable = false)
    private String code;

    @Column(nullable = false)
    private Boolean is_risk_entity;

    @Column(name = "is_enabled")
    @XmlElement
    private Boolean isEnabled = true;

    @Column(name = "created_on", nullable = false)
    @XmlElement
    private Timestamp createdOn;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RepositoryChild> childs;

    public RepositoryParent() { }

    public RepositoryParent(Long id, String name, String code, Boolean is_risk_entity, Timestamp createdOn, List<RepositoryChild> childs) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.is_risk_entity = is_risk_entity;
        this.createdOn = createdOn;
        this.childs = childs;
    }

    @PrePersist
    protected void onCreate() {
        this.createdOn = Timestamp.from(Instant.now());
    }
}
