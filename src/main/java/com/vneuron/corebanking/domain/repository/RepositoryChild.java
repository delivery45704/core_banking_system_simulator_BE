package com.vneuron.corebanking.domain.repository;
import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import javax.xml.bind.annotation.XmlElement;
import java.sql.Timestamp;
import java.time.Instant;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "child_repository")
@Getter
@Setter
public class RepositoryChild {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 40)
    @Column(nullable = false)
    private String name;

    @Size(max = 50)
    @Column(nullable = false)
    private String code;

    @Column(name = "is_enabled")
    @XmlElement
    private Boolean isEnabled = true;

    @Column(name = "created_on", nullable = false)
    @XmlElement
    private Timestamp createdOn;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "parent_id") // This column links to the parent table
    private RepositoryParent parent;

    public RepositoryChild() {}

    public RepositoryChild(Long id, String name, String code, Timestamp createdOn, RepositoryParent parent) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.createdOn = createdOn;
        this.parent = parent;
    }

    @PrePersist
    protected void onCreate() {
        this.createdOn = Timestamp.from(Instant.now());
    }
}
