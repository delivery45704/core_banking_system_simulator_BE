package com.vneuron.corebanking.domain.kyc_third_party_config;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "kyc_third_party_configuration")
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Getter
@Setter
public class KycThirdPartyConfiguration {

    @Id
    @NotBlank(message = "Server URL must not be empty")
    private String serverUrl;

    @NotBlank(message = "Auth Endpoint must not be empty")
    private String authEndpoint;

    @NotBlank(message = "Onboarding Endpoint must not be empty")
    private String onboardingEndpoint;

    @NotBlank(message = "Customer Status Endpoint must not be empty")
    private String customerStatusEndpoint;

    @NotBlank(message = "Username must not be empty")
    private String username;

    @NotBlank(message = "Password must not be empty")
    private String password;

    // Default no-argument constructor
    public KycThirdPartyConfiguration() {}

    // Parameterized constructor
    public KycThirdPartyConfiguration(String serverUrl, String authEndpoint, String onboardingEndpoint,
                                      String customerStatusEndpoint, String username, String password) {
        this.serverUrl = serverUrl;
        this.authEndpoint = authEndpoint;
        this.onboardingEndpoint = onboardingEndpoint;
        this.customerStatusEndpoint = customerStatusEndpoint;
        this.username = username;
        this.password = password;
    }
}