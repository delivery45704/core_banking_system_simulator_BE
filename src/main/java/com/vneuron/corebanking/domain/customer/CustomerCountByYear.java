package com.vneuron.corebanking.domain.customer;


public class CustomerCountByYear {
    private int year;
    private long customerCount;

    public CustomerCountByYear(int year, long customerCount) {
        this.year = year;
        this.customerCount = customerCount;
    }

    // Getters and setters
    public int getYear() {
        return year;
    }

    public long getCustomerCount() {
        return customerCount;
    }
}
