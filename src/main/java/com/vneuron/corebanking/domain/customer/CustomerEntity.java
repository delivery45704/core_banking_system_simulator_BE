package com.vneuron.corebanking.domain.customer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vneuron.corebanking.domain.account.AccountEntity;
import jakarta.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "customers")
@XmlRootElement(name = "CustomerEntity")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement
    private Long id;

    @Column(name = "created_on", nullable = false)
    @XmlElement
    private Timestamp createdOn;

    @Column(name = "is_enabled")
    @XmlElement
    private Boolean isEnabled = true;

    @Column(name = "activity")
    @JsonProperty("activity")
    @XmlElement
    private String activity;

    @Column(name = "age")
    @JsonProperty("age")
    @XmlElement
    private Integer age;

    @Column(name = "birth_date")
    @JsonProperty("birth_date")
    @XmlElement
    private String birthDate;

    @Column(name = "birth_place")
    @JsonProperty("birth_place")
    @XmlElement
    private String birthPlace;

    @Column(name = "business_name")
    @JsonProperty("business_name")
    @XmlElement
    private String businessName;

    @Column(name = "citizenship")
    @JsonProperty("citizenship")
    @XmlElement
    private String citizenship;

    @Column(name = "current_risk_rate")
    @XmlElement
    private Float currentRiskRate;

    @Column(name = "customer_status")
    @XmlElement
    private Integer customerStatus;

    @Column(name = "customer_type", columnDefinition = "integer default 5")
    @JsonProperty("customer_type")
    @XmlElement
    private Integer customerType;

    @Column(name = "eai_id", nullable = false, unique = true)
    @JsonProperty("eai_id")
    @XmlElement
    private Long eaiId;

    @Column(name = "email")
    @JsonProperty("email")
    @XmlElement
    private String email;

    @Column(name = "entity_type")
    @JsonProperty("entity_type")
    @XmlElement
    private String entityType;

    @Column(name = "fields0")
    @JsonProperty("fields0")
    @XmlElement
    private String fields0;

    @Column(name = "fields1")
    @JsonProperty("fields1")
    @XmlElement
    private String fields1;

    @Column(name = "fields2")
    @JsonProperty("fields2")
    @XmlElement
    private String fields2;

    @Column(name = "fields3")
    @JsonProperty("fields3")
    @XmlElement
    private String fields3;

    @Column(name = "first_name")
    @JsonProperty("first_name")
    @XmlElement
    private String firstName;

    @Column(name = "gender")
    @JsonProperty("gender")
    @XmlElement
    private String gender;

    @Column(name = "last_name")
    @JsonProperty("last_name")
    @XmlElement
    private String lastName;

    @Column(name = "nationality")
    @JsonProperty("nationality")
    @XmlElement
    private String nationality;

    @Column(name = "nid")
    @JsonProperty("nid")
    @XmlElement
    private String nid;

    @Column(name = "passeport")
    @JsonProperty("passeport")
    @XmlElement
    private String passeport;

    @Column(name = "profession")
    @JsonProperty("profession")
    @XmlElement
    private String profession;

    @Column(name = "risk_value")
    @XmlElement
    private Integer riskValue;

    @Column(name = "system")
    @JsonProperty("system")
    @XmlElement
    private String system;

    @Column(name = "tin")
    @JsonProperty("tin")
    @XmlElement
    private String tin;

    @Column(name = "whole_name")
    @JsonProperty("whole_name")
    @XmlElement
    private String wholeName;

    @Column(name = "form_data_id")
    @XmlElement
    private Long formDataId;

    @OneToMany(mappedBy = "customer")
    @JsonManagedReference
    private List<AccountEntity> contracts;

    // Default constructor
    public CustomerEntity() {}

    // Parameterized constructor
    public CustomerEntity(String businessName, Long id, Timestamp createdOn, Boolean isEnabled, String activity, Integer age, String birthDate, String birthPlace, String citizenship, Float currentRiskRate, Integer customerStatus, Integer customerType, Long eaiId, String email, String entityType, String fields0, String fields1, String fields2, String fields3, String firstName, String gender, String lastName, String nationality, String nid, String passeport, String profession, Integer riskValue, String system, String tin, String wholeName, Long formDataId) {
        this.businessName = businessName;
        this.id = id;
        this.createdOn = createdOn;
        this.isEnabled = isEnabled;
        this.activity = activity;
        this.age = age;
        this.birthDate = birthDate;
        this.birthPlace = birthPlace;
        this.citizenship = citizenship;
        this.currentRiskRate = currentRiskRate;
        this.customerStatus = customerStatus;
        this.customerType = customerType;
        this.eaiId = eaiId;
        this.email = email;
        this.entityType = entityType;
        this.fields0 = fields0;
        this.fields1 = fields1;
        this.fields2 = fields2;
        this.fields3 = fields3;
        this.firstName = firstName;
        this.gender = gender;
        this.lastName = lastName;
        this.nationality = nationality;
        this.nid = nid;
        this.passeport = passeport;
        this.profession = profession;
        this.riskValue = riskValue;
        this.system = system;
        this.tin = tin;
        this.wholeName = wholeName;
        this.formDataId = formDataId;
    }

    // Getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public Float getCurrentRiskRate() {
        return currentRiskRate;
    }

    public void setCurrentRiskRate(Float currentRiskRate) {
        this.currentRiskRate = currentRiskRate;
    }

    public Integer getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(Integer customerStatus) {
        this.customerStatus = customerStatus;
    }

    public Integer getCustomerType() {
        return customerType;
    }

    public void setCustomerType(Integer customerType) {
        this.customerType = customerType;
    }

    public Long getEaiId() {
        return eaiId;
    }

    public void setEaiId(Long eaiId) {
        this.eaiId = eaiId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getFields0() {
        return fields0;
    }

    public void setFields0(String fields0) {
        this.fields0 = fields0;
    }

    public String getFields1() {
        return fields1;
    }

    public void setFields1(String fields1) {
        this.fields1 = fields1;
    }

    public String getFields2() {
        return fields2;
    }

    public void setFields2(String fields2) {
        this.fields2 = fields2;
    }

    public String getFields3() {
        return fields3;
    }

    public void setFields3(String fields3) {
        this.fields3 = fields3;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getPasseport() {
        return passeport;
    }

    public void setPasseport(String passeport) {
        this.passeport = passeport;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Integer getRiskValue() {
        return riskValue;
    }

    public void setRiskValue(Integer riskValue) {
        this.riskValue = riskValue;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getWholeName() {
        return wholeName;
    }

    public void setWholeName(String wholeName) {
        this.wholeName = wholeName;
    }

    public Long getFormDataId() {
        return formDataId;
    }

    public void setFormDataId(Long formDataId) {
        this.formDataId = formDataId;
    }

    public List<AccountEntity> getContracts() {
        return contracts;
    }

    public void setContracts(List<AccountEntity> contracts) {
        this.contracts = contracts;
    }

    @PrePersist
    protected void onCreate() {
        this.createdOn = Timestamp.from(Instant.now());
    }
}
