package com.vneuron.corebanking.domain.form;

import com.vneuron.corebanking.domain.entity_type.ECustomerType;
import com.vneuron.corebanking.domain.entity_type.EntityType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="forms")
public class Form {

    @Setter
    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Setter
    @Getter
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Setter
    @Getter
    @Column(name = "is_enabled")
    private Boolean isEnabled;
    @Setter
    @Getter
    @Column(nullable = false, unique = true)
    private String code;
    @Setter
    @Getter
    @Column(columnDefinition = "text",nullable = false)
    private String content;
    @Setter
    @Getter
    private String description;
    @Setter
    @Getter
    @Column(nullable = false)
    private String name;
    @Setter
    @Getter
    private Integer status;
    @Setter
    @Getter
    private ECustomerType type;
    @Setter
    @Getter
    @Column(name = "user_id")
    private Long userId;

    @Getter
    @Setter
    @ManyToMany(mappedBy = "forms")
    private Set<EntityType> entityTypes = new HashSet<>();

}
