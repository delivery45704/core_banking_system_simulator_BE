package com.vneuron.corebanking.domain.user;

public enum ERole {
    USER,
    ADMIN,
    MODERATOR
}
