package com.vneuron.corebanking.application.repository;
import com.vneuron.corebanking.domain.repository.RepositoryChild;
import com.vneuron.corebanking.domain.repository.RepositoryParent;
import com.vneuron.corebanking.infrastructure.persistence.RepositoryChildCrudRepository;
import com.vneuron.corebanking.infrastructure.persistence.RepositoryParentCrudRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RepositoryChildServiceImpl implements RepositoryChildService {
    private final RepositoryParentCrudRepository repositoryParentCrudRepository;
    private final RepositoryChildCrudRepository repositoryChildCrudRepository;

    @Autowired
    public RepositoryChildServiceImpl(RepositoryParentCrudRepository RepositoryParentCrudRepository, RepositoryChildCrudRepository repositoryChildCrudRepository) {
        this.repositoryParentCrudRepository = RepositoryParentCrudRepository;
        this.repositoryChildCrudRepository = repositoryChildCrudRepository;
    }

    @Override
    public List<RepositoryChild> getAllRepositoryChilds() {
        return (List<RepositoryChild>) repositoryChildCrudRepository.findAll();
    }

    @Override
    public RepositoryChild getRepositoryChildById(Long id) {
        Optional<RepositoryChild> optionalRep = repositoryChildCrudRepository.findById(Math.toIntExact(id));
        return optionalRep.orElse(null);
    }

    @Override
    public List<RepositoryChild> getRepositoryChildsById(String id) {
        RepositoryParent parent = repositoryParentCrudRepository.findById(Integer.valueOf(id))
                .orElseThrow(() -> new EntityNotFoundException("Parent not found"));
        return repositoryChildCrudRepository.findByParentId(parent.getId());
    }


    @Override
    public RepositoryChild saveRepositoryChild(RepositoryChild repository) {
        return repositoryChildCrudRepository.save(repository);
    }

    @Override
    public void deleteRepositoryChild(Long id) {
        repositoryChildCrudRepository.deleteById(Math.toIntExact(id));
    }
}
