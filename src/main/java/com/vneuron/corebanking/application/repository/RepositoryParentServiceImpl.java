package com.vneuron.corebanking.application.repository;
import com.vneuron.corebanking.domain.repository.RepositoryParent;
import com.vneuron.corebanking.infrastructure.persistence.RepositoryParentCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RepositoryParentServiceImpl implements RepositoryParentService {
    private final RepositoryParentCrudRepository repositoryParentCrudRepository;

    @Autowired
    public RepositoryParentServiceImpl(RepositoryParentCrudRepository RepositoryParentCrudRepository) {
        this.repositoryParentCrudRepository = RepositoryParentCrudRepository;
    }


    @Override
    public List<RepositoryParent> getAllRepositorys() {
        return (List<RepositoryParent>) repositoryParentCrudRepository.findAll();
    }

    @Override
    public RepositoryParent getRepositoryById(Long id) {
        Optional<RepositoryParent> optionalRep = repositoryParentCrudRepository.findById(Math.toIntExact(id));
        return optionalRep.orElse(null);
    }

    @Override
    public RepositoryParent saveRepository(RepositoryParent repository) {
        return repositoryParentCrudRepository.save(repository);
    }

    @Override
    public void deleteRepository(Long id) {
        repositoryParentCrudRepository.deleteById(Math.toIntExact(id));
    }
}
