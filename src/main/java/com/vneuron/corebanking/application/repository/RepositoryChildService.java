package com.vneuron.corebanking.application.repository;

import com.vneuron.corebanking.domain.repository.RepositoryChild;

import java.util.List;
import java.util.Optional;

public interface RepositoryChildService {

    List<RepositoryChild> getAllRepositoryChilds();
    RepositoryChild getRepositoryChildById(Long id);

    List<RepositoryChild> getRepositoryChildsById(String id);

    RepositoryChild saveRepositoryChild(RepositoryChild repository);

    void deleteRepositoryChild(Long id);

}
