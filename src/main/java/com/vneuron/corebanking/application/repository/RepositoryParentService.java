package com.vneuron.corebanking.application.repository;

import com.vneuron.corebanking.domain.repository.RepositoryParent;
import java.util.List;

public interface RepositoryParentService {

    List<RepositoryParent> getAllRepositorys();
    RepositoryParent getRepositoryById(Long id);
    RepositoryParent saveRepository(RepositoryParent Repository);
    void deleteRepository(Long id);

}
