package com.vneuron.corebanking.application.kyc_third_party_config;

import com.vneuron.corebanking.domain.kyc_third_party_config.KycThirdPartyConfiguration;


public interface KycConfigurationService {

    KycThirdPartyConfiguration getKycConfigurations();
    void saveKycConfiguration(KycThirdPartyConfiguration configuration);


}

