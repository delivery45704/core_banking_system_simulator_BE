package com.vneuron.corebanking.application.kyc_third_party_config;

import com.vneuron.corebanking.domain.kyc_third_party_config.KycThirdPartyConfiguration;
import com.vneuron.corebanking.infrastructure.persistence.KycConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


@Service
public class KycConfigurationServiceImpl implements KycConfigurationService {

    @Autowired
    KycConfigurationRepository repository;


    @Override
    @Cacheable("kycConfigurations")
    public KycThirdPartyConfiguration getKycConfigurations() {
        return repository.findFirstBy();
    }

    @Override
    public void saveKycConfiguration(KycThirdPartyConfiguration configuration) {
        // Since we want to ensure only one configuration exists, delete existing one (if any)
        repository.deleteAll();
        repository.save(configuration);
    }
}
