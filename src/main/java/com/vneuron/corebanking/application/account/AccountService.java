package com.vneuron.corebanking.application.account;
import com.vneuron.corebanking.domain.account.AccountEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface AccountService {
    List<AccountEntity> getAllAccounts();
    AccountEntity getAccountById(Long id);
    AccountEntity saveAccount(AccountEntity user);
    void deleteAccount(Long id);
}
