package com.vneuron.corebanking.application.account;

import com.vneuron.corebanking.domain.account.AccountEntity;
import com.vneuron.corebanking.infrastructure.persistence.AccountCrudRepository;
import com.vneuron.corebanking.infrastructure.persistence.AccountRepository;
import com.vneuron.corebanking.presentation.transform.FormMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<AccountEntity> getAllAccounts() {
        return accountRepository.findAll();
    }

    public AccountEntity getAccountById(Long id) {
        Optional<AccountEntity> optionalAccount = accountRepository.findById((long) Math.toIntExact(id));
        return optionalAccount.orElse(null);
    }

    public AccountEntity saveAccount(AccountEntity account) {
        return accountRepository.save(account);
    }

    public void deleteAccount(Long id) {
        accountRepository.deleteById((long) Math.toIntExact(id));
    }
}
