package com.vneuron.corebanking.application.form;

import com.vneuron.corebanking.presentation.dto.FormEntityMappingDTO;
import com.vneuron.corebanking.presentation.utils.ResourceNotFoundException;

import java.util.List;

public interface FormEntityTypeMapService {


    List<FormEntityMappingDTO> getAllMappings();
    void mapFormToEntityType(String formCode, String entityTypeName);
    boolean isMappingExists(String formCode, String entityTypeName);

    void deleteMapping(String formCode, String entityTypeName) throws ResourceNotFoundException;
}
