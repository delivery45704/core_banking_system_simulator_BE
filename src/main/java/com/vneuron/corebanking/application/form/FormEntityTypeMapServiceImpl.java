package com.vneuron.corebanking.application.form;

import com.vneuron.corebanking.domain.entity_type.EntityType;
import com.vneuron.corebanking.domain.form.Form;
import com.vneuron.corebanking.infrastructure.persistence.EntityTypeRepository;
import com.vneuron.corebanking.infrastructure.persistence.FormRepository;
import com.vneuron.corebanking.presentation.dto.FormEntityMappingDTO;
import com.vneuron.corebanking.presentation.utils.EntityNotFoundException;
import com.vneuron.corebanking.presentation.utils.FormNotFoundException;
import com.vneuron.corebanking.presentation.utils.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FormEntityTypeMapServiceImpl implements FormEntityTypeMapService {

    @Autowired
    FormRepository formRepository;

    @Autowired
    EntityTypeRepository entityTypeRepository;


    // Method to get all mappings between Form and EntityType
    public List<FormEntityMappingDTO> getAllMappings() {
        List<EntityType> entityTypes = entityTypeRepository.findAll();

        List<FormEntityMappingDTO> mappings = new ArrayList<>();
        for (EntityType entityType : entityTypes) {
            for (Form form : entityType.getForms()) {
                mappings.add(new FormEntityMappingDTO(form.getCode(), entityType.getName()));
            }
        }
        return mappings;
    }

    // Method to map Form to EntityType
    public void mapFormToEntityType(String formCode, String entityTypeName) {
        Form form = formRepository.findFormByCode(formCode)
                .orElseThrow(() -> new FormNotFoundException("Form not found"));

        EntityType entityType = entityTypeRepository.findByName(entityTypeName)
                .orElseThrow(() -> new EntityNotFoundException("Entity Type not found"));

        entityType.getForms().add(form);
        entityTypeRepository.save(entityType);
    }

    @Override
    public boolean isMappingExists(String formCode, String entityTypeName) {
        Optional<Form> form = formRepository.findFormByCode(formCode);
        return form.map(value -> value.getEntityTypes().stream()
                .anyMatch(entityType -> entityType.getName().equals(entityTypeName))).orElse(false);
    }


    public void deleteMapping(String formCode, String entityTypeName) {
        // Find the form and entity type
        Form form = formRepository.findFormByCode(formCode)
                .orElseThrow(() -> new EntityNotFoundException("Form not found"));

        EntityType entityType = entityTypeRepository.findByName(entityTypeName)
                .orElseThrow(() -> new EntityNotFoundException("EntityType not found"));

        // Remove the entityType from the form's collection
        if (form.getEntityTypes().remove(entityType)) {
            // Also remove the form from the entityType's collection
            entityType.getForms().remove(form);

            // Save changes to both entities
            formRepository.save(form);
            entityTypeRepository.save(entityType);
        } else {
            throw new EntityNotFoundException("Mapping not found for deletion");
        }

        // Log after modification
        System.out.println("After deletion - Form: " + form.getEntityTypes() + ", EntityType: " + entityType.getForms());
    }
}
