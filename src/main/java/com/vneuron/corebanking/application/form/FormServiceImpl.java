package com.vneuron.corebanking.application.form;

import com.vneuron.corebanking.domain.form.Form;
import com.vneuron.corebanking.infrastructure.persistence.FormRepository;
import com.vneuron.corebanking.presentation.controller.AuthController;
import com.vneuron.corebanking.presentation.dto.FormDto;
import com.vneuron.corebanking.presentation.transform.FormMapper;
import com.vneuron.corebanking.presentation.utils.FormNotFoundException;
import com.vneuron.corebanking.presentation.utils.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FormServiceImpl implements FormService {

    @Autowired
    private FormRepository formRepository;

    private final static Logger logger = LoggerFactory.getLogger(FormServiceImpl.class);

    @Override
    public FormDto saveForm(FormDto formDto) {
        Form form = FormMapper.toEntity(formDto);
        Form savedForm = formRepository.save(form);
        return FormMapper.toDto(savedForm);
    }

    @Override
    public List<FormDto> getAllForms() {
        return formRepository.findAll().stream()
                .map(FormMapper::toDto)
                .collect(Collectors.toList());

    }

    @Override
    public Page<FormDto> getAllForms(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Form> forms = this.formRepository.findAll(pageable);
        logger.debug("size= "+forms.getTotalElements());
        logger.info("size= "+forms.getTotalElements());
        return forms.map(FormMapper::toDto);
    }

    @Override
    public FormDto getFormById(Long id) {
        Form form = formRepository.findById(id).orElse(null);
        if (form == null) {
            return null;
        }
        return FormMapper.toDto(form);
    }

    @Override
    public void deleteForm(Long id) {
        formRepository.deleteById(id);
    }

    @Override
    public void deleteForms(List<Long> ids) {
        for (Long id : ids) {
            deleteForm(id);
        }
    }
    @Override
    public FormDto updateForm(Long id, FormDto formDto) {
        Optional<Form> existingFormOptional = formRepository.findById(id);
        if (existingFormOptional.isPresent()) {
            Form existingForm = FormMapper.toEntity(formDto);
            existingForm.setId(id);
            Form updatedForm = formRepository.save(existingForm);
            return FormMapper.toDto(updatedForm);
        }else {
            return null;
        }
    }


    @Override
    public boolean isCodeExist(String code) {
        return formRepository.existsByCode(code);
    }
    @Override
    public Form findByCode(String code) throws ResourceNotFoundException {
        return formRepository.findFormByCode(code)
                .orElseThrow(() -> new ResourceNotFoundException("No form by this code"));
    }
}
