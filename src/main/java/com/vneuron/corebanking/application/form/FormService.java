package com.vneuron.corebanking.application.form;

import com.vneuron.corebanking.domain.form.Form;
import com.vneuron.corebanking.presentation.dto.FormDto;
import com.vneuron.corebanking.presentation.utils.ResourceNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface FormService {


    public FormDto saveForm(FormDto formDto);
    public List<FormDto> getAllForms();
    public Page<FormDto> getAllForms(int page, int size);
    public FormDto getFormById(Long id);
    public void deleteForm(Long id);
    public void deleteForms(List<Long> ids);

    public FormDto updateForm(Long id, FormDto formDto);

    boolean isCodeExist(String code);
    Form findByCode(String code) throws ResourceNotFoundException;
}

