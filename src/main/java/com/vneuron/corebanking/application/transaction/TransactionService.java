package com.vneuron.corebanking.application.transaction;
import com.vneuron.corebanking.domain.transaction.Transaction;
import com.vneuron.corebanking.presentation.dto.TransactionDto;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TransactionService {
    List<TransactionDto> getAllTransactions();
    Page<TransactionDto> getAllTransactions(int page, int size);
    TransactionDto getTransactionById(Long id);
    TransactionDto saveTransaction(TransactionDto transactionDto);
    void deleteTransaction(Long id);
}
