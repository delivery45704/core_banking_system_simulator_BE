package com.vneuron.corebanking.application.transaction;

import com.vneuron.corebanking.domain.form.Form;
import com.vneuron.corebanking.domain.transaction.Transaction;
import com.vneuron.corebanking.infrastructure.persistence.TransactionRepository;
import com.vneuron.corebanking.presentation.dto.FormDto;
import com.vneuron.corebanking.presentation.dto.TransactionDto;
import com.vneuron.corebanking.presentation.transform.FormMapper;
import com.vneuron.corebanking.presentation.transform.TransactionMapper;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService{

    @Autowired
    private TransactionRepository transactionRepository;

    @PersistenceContext
    private EntityManager entityManager;

    public List<TransactionDto> getAllTransactions() {

        return transactionRepository.findAll().stream()
                .map(TransactionMapper::toDto)
                .collect(Collectors.toList());
    }

    /**
     * get all transaction by using pagination to optimize response time
     * @param page: containing the page number
     * @param size: containing the size of each page
     * @return the desired page
     */
    @Override
    public Page<TransactionDto> getAllTransactions(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Transaction> forms = transactionRepository.findAll(pageable);
        return forms.map(TransactionMapper::toDto);
    }

    public TransactionDto getTransactionById(Long id){
        Transaction transaction = transactionRepository.findById(id).orElse(null);
        if (transaction == null) {
            return null;
        }
        return TransactionMapper.toDto(transaction);
    }
    public TransactionDto saveTransaction(TransactionDto transactionDto) {
        Transaction transaction = TransactionMapper.toEntity(transactionDto,entityManager);
        Transaction savedTransaction = transactionRepository.save(transaction);
        return TransactionMapper.toDto(savedTransaction);
    }

    public void deleteTransaction(Long id) {
        transactionRepository.deleteById(id);
    }
}
