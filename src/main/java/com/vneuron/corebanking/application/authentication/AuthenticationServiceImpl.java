package com.vneuron.corebanking.application.authentication;

import com.vneuron.corebanking.infrastructure.security.jwt.JwtUtils;
import com.vneuron.corebanking.infrastructure.security.services.UserDetailsImpl;
import com.vneuron.corebanking.presentation.dto.UserLoginResponseDto;
import com.vneuron.corebanking.presentation.transform.UserMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    AuthenticationManager authenticationManager;
    JwtUtils jwtUtils;

    public AuthenticationServiceImpl(AuthenticationManager authenticationManager, JwtUtils jwtUtils) {
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
    }

    public UserLoginResponseDto authenticateUser(String username, String password) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password)
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return UserMapper.toUserLoginResponseModel(jwt, userDetails);

    }
}
