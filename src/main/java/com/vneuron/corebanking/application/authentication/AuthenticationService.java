package com.vneuron.corebanking.application.authentication;

import com.vneuron.corebanking.presentation.dto.UserLoginResponseDto;

public interface AuthenticationService {

    public UserLoginResponseDto authenticateUser(String username, String password);
}
