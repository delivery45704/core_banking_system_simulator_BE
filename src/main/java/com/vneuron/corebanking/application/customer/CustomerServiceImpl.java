package com.vneuron.corebanking.application.customer;

import com.vneuron.corebanking.domain.customer.CustomerCountByYear;
import com.vneuron.corebanking.domain.customer.CustomerEntity;
import com.vneuron.corebanking.infrastructure.persistence.CustomerCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerCrudRepository customerCrudRepository;

    @Autowired
    public CustomerServiceImpl(CustomerCrudRepository customerCrudRepository) {
        this.customerCrudRepository = customerCrudRepository;
    }

    @Override
    public CustomerEntity getLastInsertedCustomer() {
        return customerCrudRepository.findTopByOrderByIdDesc();
    }

    @Override
    public CustomerEntity findByEaiId(Long eaiId) {
        return customerCrudRepository.findByEaiId(String.valueOf(eaiId));
    }

    @Override
    public int findByEntityType(String entity_type) {
        return customerCrudRepository.findByEntityType(String.valueOf(entity_type));
    }

    @Override
    public List<CustomerCountByYear> findCountCustomersByYear() {
        return customerCrudRepository.findCountCustomersByYear();
    }

    @Override
    public List<CustomerEntity> getAllCustomers() {
        return (List<CustomerEntity>) customerCrudRepository.findAll();
    }

    @Override
    public CustomerEntity getCustomerById(Long id) {
        Optional<CustomerEntity> optionalUser = customerCrudRepository.findById(Math.toIntExact(id));
        return optionalUser.orElse(null);
    }

    @Override
    public CustomerEntity saveCustomer(CustomerEntity customer) {
        return customerCrudRepository.save(customer);
    }

    @Override
    public void deleteCustomer(Long id) {
        customerCrudRepository.deleteById(Math.toIntExact(id));
    }

}
