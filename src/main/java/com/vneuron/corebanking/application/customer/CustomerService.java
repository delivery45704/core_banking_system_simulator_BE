package com.vneuron.corebanking.application.customer;

import com.vneuron.corebanking.domain.customer.CustomerCountByYear;
import com.vneuron.corebanking.domain.customer.CustomerEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {
    CustomerEntity getLastInsertedCustomer();
    CustomerEntity findByEaiId(Long eaiId);

    int findByEntityType(String entity_type);
    List<CustomerCountByYear> findCountCustomersByYear();

    List<CustomerEntity> getAllCustomers();
    CustomerEntity getCustomerById(Long id);
    CustomerEntity saveCustomer(CustomerEntity customer);
    void deleteCustomer(Long id);
}
