package com.vneuron.corebanking.application.user;
import com.vneuron.corebanking.domain.user.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    List<UserEntity> getAllUsers();
    UserEntity getUserById(Long id);
    UserEntity saveUser(UserEntity user);

    void deleteUser(Long id);
}
