package com.vneuron.corebanking.application.user;

import com.vneuron.corebanking.domain.user.UserEntity;
import com.vneuron.corebanking.infrastructure.persistence.UserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{
    private final UserCrudRepository userCrudRepository;

    @Autowired
    public UserServiceImpl(UserCrudRepository userCrudRepository) {
        this.userCrudRepository = userCrudRepository;
    }

    public List<UserEntity> getAllUsers() {
        return (List<UserEntity>) userCrudRepository.findAll();
    }

    public UserEntity getUserById(Long id) {
        Optional<UserEntity> optionalUser = userCrudRepository.findById(Math.toIntExact(id));
        return optionalUser.orElse(null); // Handle null if user is not found
    }

    public UserEntity saveUser(UserEntity user) {
        return userCrudRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userCrudRepository.deleteById(Math.toIntExact(id));
    }

    public void deleteUser(Integer id) {
        userCrudRepository.deleteById(id);
    }
}
