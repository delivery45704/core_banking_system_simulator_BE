package com.vneuron.corebanking.application.entity_type;

import com.vneuron.corebanking.domain.entity_type.EntityType;
import com.vneuron.corebanking.presentation.dto.EntityTypeDto;

import java.util.List;

public interface EntityTypeService {

    List<EntityTypeDto> getAllEntityTypes();

    EntityTypeDto saveEntityType(EntityTypeDto entityTypeDto);

    void deleteEntityType(String name);

    EntityType findByName(String name);
}
