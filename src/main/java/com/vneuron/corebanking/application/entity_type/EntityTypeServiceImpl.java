package com.vneuron.corebanking.application.entity_type;

import com.vneuron.corebanking.domain.entity_type.EntityType;
import com.vneuron.corebanking.infrastructure.persistence.EntityTypeRepository;
import com.vneuron.corebanking.presentation.dto.EntityTypeDto;
import com.vneuron.corebanking.presentation.transform.EntityTypeMapper;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EntityTypeServiceImpl implements EntityTypeService{

    @Autowired
    private EntityTypeRepository repository;
    @Override
    public List<EntityTypeDto> getAllEntityTypes() {
        return repository.findAll()
                .stream().map(EntityTypeMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public EntityTypeDto saveEntityType(EntityTypeDto entityTypeDto) {
        EntityType mEntityType = EntityTypeMapper.toEntity(entityTypeDto);
        EntityType savedEntityType = repository.save(mEntityType);
        return EntityTypeMapper.toDto(savedEntityType);
    }

    @Override
    public void deleteEntityType(String name) {
        EntityType mEntityType = repository.findByName(name).orElseThrow(() -> new EntityNotFoundException("Entity type is not available"));
        if(mEntityType!= null){
            repository.delete(mEntityType);
        }else {
            throw new EntityNotFoundException("EntityType with name " + name + " not found.");
        }
    }
    @Override
    public EntityType findByName(String name) {
        return repository.findByName(name).orElseThrow(() -> new EntityNotFoundException("Entity type is not available"));
    }
}
