package com.vneuron.corebanking.application.registration;


import com.vneuron.corebanking.domain.user.ERole;
import com.vneuron.corebanking.domain.user.Role;
import com.vneuron.corebanking.domain.user.UserEntity;
import com.vneuron.corebanking.infrastructure.persistence.RoleRepository;
import com.vneuron.corebanking.infrastructure.persistence.UserRepository;
import com.vneuron.corebanking.presentation.dto.UserRegistrationResponseDto;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class RegistrationServiceImpl implements RegistrationService{

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public RegistrationServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }
    public UserRegistrationResponseDto registerUser(UserEntity userEntity, Set<String> strRoles){
        if (userRepository.existsByUsername(userEntity.getUsername())) {
            return new UserRegistrationResponseDto("Error: username is already taken!");
        }

        if (userRepository.existsByEmail(userEntity.getEmail())) {
            return new UserRegistrationResponseDto("Error: Email already in use!");
        }

        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "ADMIN" -> {
                        Role adminRole = roleRepository.findByName(ERole.ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);
                    }
                    case "MODERATOR" -> {
                        Role modRole = roleRepository.findByName(ERole.MODERATOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);
                    }
                    default -> {
                        Role userRole = roleRepository.findByName(ERole.USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                    }
                }
            });
        }

        userEntity.setRoles(roles);
        userRepository.save(userEntity);

        return new UserRegistrationResponseDto("User registered successfully!");
    }


}
