package com.vneuron.corebanking.application.registration;

import com.vneuron.corebanking.domain.user.UserEntity;
import com.vneuron.corebanking.presentation.dto.UserRegistrationResponseDto;

import java.util.Set;

public interface RegistrationService {

    public UserRegistrationResponseDto registerUser(UserEntity userEntity, Set<String> strRoles);
}
