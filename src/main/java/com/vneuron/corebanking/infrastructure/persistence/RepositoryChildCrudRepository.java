package com.vneuron.corebanking.infrastructure.persistence;

import com.vneuron.corebanking.domain.repository.RepositoryChild;
import com.vneuron.corebanking.domain.repository.RepositoryParent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RepositoryChildCrudRepository extends CrudRepository<RepositoryChild, Integer> {
    @Query("SELECT c FROM RepositoryChild c WHERE c.parent.id = :id")
    List<RepositoryChild> findByParentId(@Param("id") Long id);

}
