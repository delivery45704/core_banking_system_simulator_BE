package com.vneuron.corebanking.infrastructure.persistence;

import com.vneuron.corebanking.domain.repository.RepositoryChild;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryChildRepository extends JpaRepository<RepositoryChild,Long> {
}
