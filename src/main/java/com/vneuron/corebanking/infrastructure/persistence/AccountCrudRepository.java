package com.vneuron.corebanking.infrastructure.persistence;

import com.vneuron.corebanking.domain.account.AccountEntity;
import com.vneuron.corebanking.domain.user.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface AccountCrudRepository extends CrudRepository<AccountEntity, Integer> {
}
