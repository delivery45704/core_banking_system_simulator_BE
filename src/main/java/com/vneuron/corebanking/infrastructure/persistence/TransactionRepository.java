package com.vneuron.corebanking.infrastructure.persistence;
import com.vneuron.corebanking.domain.transaction.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
}
