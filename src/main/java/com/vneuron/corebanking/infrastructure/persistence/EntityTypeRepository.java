package com.vneuron.corebanking.infrastructure.persistence;

import com.vneuron.corebanking.domain.entity_type.EntityType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EntityTypeRepository extends JpaRepository<EntityType,String> {
    Optional<EntityType> findByName(String name);


}
