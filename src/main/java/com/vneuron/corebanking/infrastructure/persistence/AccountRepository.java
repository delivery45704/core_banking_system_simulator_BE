package com.vneuron.corebanking.infrastructure.persistence;

import com.vneuron.corebanking.domain.account.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountEntity,Long> {
}
