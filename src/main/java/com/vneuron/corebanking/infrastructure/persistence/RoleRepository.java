package com.vneuron.corebanking.infrastructure.persistence;

import com.vneuron.corebanking.domain.user.ERole;
import com.vneuron.corebanking.domain.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {


    Optional<Role> findByName(ERole name);


}
