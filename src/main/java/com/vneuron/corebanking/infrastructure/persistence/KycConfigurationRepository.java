package com.vneuron.corebanking.infrastructure.persistence;

import com.vneuron.corebanking.domain.kyc_third_party_config.KycThirdPartyConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KycConfigurationRepository extends JpaRepository<KycThirdPartyConfiguration,Void> {

    KycThirdPartyConfiguration findFirstBy();
}
