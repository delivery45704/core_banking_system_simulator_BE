package com.vneuron.corebanking.infrastructure.persistence;

import com.vneuron.corebanking.domain.user.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserCrudRepository extends CrudRepository<UserEntity, Integer> {
    List<UserEntity> findByUsername(String username);
}
