package com.vneuron.corebanking.infrastructure.persistence;

import com.vneuron.corebanking.domain.customer.CustomerCountByYear;
import com.vneuron.corebanking.domain.customer.CustomerEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CustomerCrudRepository extends CrudRepository<CustomerEntity, Integer> {

    @Query("SELECT c FROM CustomerEntity c ORDER BY c.id DESC limit 1")
    CustomerEntity findTopByOrderByIdDesc();

    @Query("SELECT c FROM CustomerEntity c WHERE c.eaiId = :eaiId")
    CustomerEntity findByEaiId(@Param("eaiId") String eaiId);

    @Query("SELECT count(c) FROM CustomerEntity c WHERE c.entityType = :entity_type")
    int findByEntityType(@Param("entity_type") String entity_type);

    @Query("SELECT new com.vneuron.corebanking.domain.customer.CustomerCountByYear(CAST(EXTRACT(YEAR FROM c.createdOn) AS int), COUNT(c)) " +
            "FROM CustomerEntity c " +
            "GROUP BY EXTRACT(YEAR FROM c.createdOn) " +
            "ORDER BY EXTRACT(YEAR FROM c.createdOn)")
    List<CustomerCountByYear> findCountCustomersByYear();
}
