package com.vneuron.corebanking.infrastructure.persistence;

import com.vneuron.corebanking.domain.repository.RepositoryParent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryParentRepository extends JpaRepository<RepositoryParent,Long> {

}
