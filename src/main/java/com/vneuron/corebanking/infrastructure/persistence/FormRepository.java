package com.vneuron.corebanking.infrastructure.persistence;


import com.vneuron.corebanking.domain.form.Form;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FormRepository extends JpaRepository<Form,Long> {

    boolean existsByCode(String code);
    boolean findByCode(String code);

    Optional<Form> findFormByCode(String code);  // Retrieves the Form entity by code
}
