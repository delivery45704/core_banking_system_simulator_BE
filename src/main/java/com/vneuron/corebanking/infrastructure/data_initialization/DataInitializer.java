package com.vneuron.corebanking.infrastructure.data_initialization;

import com.vneuron.corebanking.application.account.AccountService;
import com.vneuron.corebanking.application.customer.CustomerService;
import com.vneuron.corebanking.application.entity_type.EntityTypeService;
import com.vneuron.corebanking.domain.customer.CustomerEntity;
import com.vneuron.corebanking.domain.entity_type.ECustomerType;
import com.vneuron.corebanking.domain.entity_type.EntityType;
import com.vneuron.corebanking.domain.user.ERole;
import com.vneuron.corebanking.domain.user.Role;
import com.vneuron.corebanking.domain.user.UserEntity;
import com.vneuron.corebanking.infrastructure.persistence.EntityTypeRepository;
import com.vneuron.corebanking.infrastructure.persistence.RoleRepository;
import com.vneuron.corebanking.infrastructure.persistence.UserRepository;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.math.BigDecimal;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVParser;

import com.vneuron.corebanking.domain.account.AccountEntity;

@Component
public class DataInitializer implements CommandLineRunner {

    @Autowired
    private Environment env;

    @Value("${admin.password}")
    private String adminPassword;

    @Value("${contracts.path}")
    private String accountsPath;

    private static final Logger logger = Logger.getLogger(DataInitializer.class.getName());

    private static final String[] EXPECTED_HEADERS_CONTRACT = {"account_status", "contract_balance", "currency", "num_contract", "opened_date", "termination_date", "customer_id"};

    private final RoleRepository roleRepository;

    private final UserRepository userRepository;

    private final PasswordEncoder encoder;

    @Autowired
    private CustomerService customerService;

    private final AccountService accountService;

    @Autowired
    EntityTypeRepository entityTypeRepository;


    public DataInitializer(RoleRepository roleRepository, UserRepository userRepository, PasswordEncoder encoder, AccountService accountService, CustomerService customerService) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.encoder = encoder;
        this.accountService = accountService;
        this.customerService = customerService;
    }

    @Override
    public void run(String... args) throws Exception {
        initRoles();
        createAccounts();
        initEntityTypes();
    }

    private void initRoles() {
        if(roleRepository.count() ==0){
            // Create default roles if they don't exist
            Role roleAdmin = new Role(ERole.ADMIN);
            Role roleUser = new Role(ERole.USER);
            Role roleModerator = new Role(ERole.MODERATOR);

            roleRepository.saveAll(List.of(roleAdmin,roleModerator,roleUser));

            iniUser(roleAdmin);
        }
    }
    private void iniUser(Role roleAdmin) {
        if(userRepository.count() ==0){
            Set<Role> roles = new HashSet<>();
            roles.add(roleAdmin);
            // Create user with roles
            UserEntity userAdmin = new UserEntity(1L, "admin", "vneuron@gmai.com", encoder.encode(adminPassword), "admin", "vneuron", true, roles);
            userRepository.save(userAdmin);
        }
    }

    private void createAccounts(){
        if (!Objects.equals(accountsPath, "")) {
            Path path = Paths.get(accountsPath);
            try {
                Files.list(path)
                        .filter(file -> file.getFileName().toString().startsWith("contracts"))
                        .forEach(filePath -> processFile(filePath,"contract")); // Process each file
            } catch (IOException e) {
                e.printStackTrace(); // Handle exception
            }
        }
    }

    private void processFile(Path filePath,String type) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (type.equals("contract")) {
            System.out.println("Processing file (contract) : " + filePath.toString());
            try (FileReader reader = new FileReader(filePath.toFile())) {
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withDelimiter(';'));
                if (validateHeaders(csvParser.getHeaderNames())) {
                    for (CSVRecord record : csvParser) {
                        AccountEntity accountEntity = new AccountEntity();

                        // Mapper les champs spécifiques du fichier CSV vers AccountEntity avec switch-case
                        for (String headerName : csvParser.getHeaderNames()) {
                            switch (headerName) {
                                case "account_status":
                                    accountEntity.setAccountStatus(getValueOrNull(record, "account_status"));
                                    break;
                                case "contract_balance":
                                    accountEntity.setContractBalance(new BigDecimal(getValueOrNull(record, "contract_balance")));
                                    break;
                                case "currency":
                                    accountEntity.setCurrency(getValueOrNull(record, "currency"));
                                    break;
                                case "num_contract":
                                    accountEntity.setNumContract(getValueOrNull(record, "num_contract"));
                                    break;
                                case "opened_date":
                                    String openedDateStr = getValueOrNull(record, "opened_date");
                                    if (openedDateStr != null && !openedDateStr.isEmpty()) {
                                        Date parsedDate = dateFormat.parse(openedDateStr);
                                        Timestamp timestamp = new Timestamp(parsedDate.getTime());
                                        accountEntity.setOpenedDate(timestamp);
                                    }
                                    break;
                                case "termination_date":
                                    String terminationDateStr = getValueOrNull(record, "termination_date");
                                    if (terminationDateStr != null && !terminationDateStr.isEmpty()) {
                                        Date parsedDate = dateFormat.parse(terminationDateStr);
                                        Timestamp termination_times_stamp = new Timestamp(parsedDate.getTime());
                                        accountEntity.setTerminationDate(termination_times_stamp);
                                    }
                                    break;
                                case "customer_id":
                                    CustomerEntity cust_id = customerService.getCustomerById((long) Integer.parseInt(getValueOrNull(record, "customer_id")));
                                    accountEntity.setCustomer(cust_id);
                                    break;
                                default:
                                    break;
                            }
                        }
                        accountService.saveAccount(accountEntity);
                    }
                }
                else {
                    logger.log(Level.SEVERE,"The file structure is incorrect : "+filePath.toString());
                }
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            System.out.println("End of processing file (contract) : " + filePath.toString());
            moveFile(filePath.toString());
        }
    }

    private void moveFile(String filePath) {
        try {
            Path sourcePath = Paths.get(filePath);
            Path parentDirectory = sourcePath.getParent();
            Path backupDirectory = parentDirectory.resolve("backup_accounts");
            if (!Files.exists(backupDirectory)) {
                Files.createDirectories(backupDirectory);
            }
            Path targetFilePath = backupDirectory.resolve(sourcePath.getFileName());
            Files.move(sourcePath, targetFilePath, StandardCopyOption.REPLACE_EXISTING);
            System.out.println("File moved successfully to " + targetFilePath.toString());
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("An error occurred while moving the file.");
        }
    }

    private boolean validateHeaders(List<String> actualHeaders) {
        String[] actualHeadersArray = actualHeaders.toArray(new String[0]);
        if (!arrayEquals(EXPECTED_HEADERS_CONTRACT, actualHeadersArray)) {
            return false;
        }
        return true;
    }

    private boolean arrayEquals(String[] expected, String[] actual) {
        if (expected.length != actual.length) {
            return false;
        }
        for (int i = 0; i < expected.length; i++) {
            String actualTrimmed = actual[i].trim();
            if (actualTrimmed.charAt(0) == '\uFEFF') {
                actualTrimmed = actualTrimmed.substring(1); // Supprime le caractère BOM
            }
            if (!expected[i].trim().equals(actualTrimmed)) {
                return false;
            }
        }
        return true;
    }

    private String getValueOrNull(CSVRecord record, String headerName) {
        String value = record.get(headerName);
        return (value != null && !value.isEmpty()) ? value : null;
    }

    /**
     * initiate the default entity types PP and PM
     */
    private void initEntityTypes(){
        if (entityTypeRepository.count() == 0){
            EntityType PP = new EntityType("PP", ECustomerType.PERSON);
            EntityType PM = new EntityType("PM", ECustomerType.ENTITY);
            entityTypeRepository.saveAll(List.of(PP,PM));
        }
    }
}
